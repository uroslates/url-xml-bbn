# Boilerplate Project

* Node based backend (express)
* Bower featured frontend dependency management
 * Require basee js depenency management, Backbone.js
* Grunt based build tool


## Intallation

$ npm install

$ bower install


# Starting Application

$ npm start

And after that visit http://localhost:3000/



## Author
[Uros Lates](http://uroslates.com)