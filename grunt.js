/*global module:false*/
module.exports = function(grunt) {

  // Configuration
  grunt.initConfig({

      publicJsDir: 'public/js'
    , publicCssDir: 'public/css'
    , publicScssDir: 'public/scss'
    , publicImgDir: 'public/img'
    , buildDir: 'public/dist'

    , min: {
      build: {
        src: '<%= buildDir %>/js/main.js'
        , dest: '<%= buildDir %>/js/main.js'
      }
    }

    , uglify: {
      mangle: {toplevel: true}
      , squeeze: {dead_code: false}
      , codegen: {quote_keys: true}
    }

    , concat: {
      css: {
        src: ['<%= publicCssDir %>/*.css']
        , dest: '<%= buildDir %>/css/main.css'
      }
    }

    , requirejs: {
      production: {
        options: {
          baseUrl: "<%= publicJsDir %>/"
          , mainConfigFile: "<%= publicJsDir %>/main.js"
          , out: "<%= buildDir %>/js/main.js"
          , name:'main'
        }
      }
    }

    , compass: {
      dev: {
        src: '<%= publicScssDir %>',
        dest: '<%= publicCssDir %>',
        linecomments: true,
        debugsass: true,
        images: '<%= publicImgDir %>',
        relativeassets: true
      }
      , prod: {
        src: '<%= publicScssDir %>',
        dest: '<%= publicCssDir %>',
        outputstyle: 'compressed',
        linecomments: false,
        forcecompile: true,
        debugsass: false,
        images: '<%= publicImgDir %>',
        relativeassets: true
      }
    }

  });

  // Tasks loading
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-compass');

  // Tasks registration
  grunt.registerTask('default', ['requirejs', 'compass:dev']);
  grunt.registerTask('build', ['requirejs', 'compass:prod', 'concat:css']);

};
