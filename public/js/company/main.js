define([
	'backbone'
	, './models/Company'
	, './collections/Companies'
	, './views/Companies'
	, './views/Search'
	, './views/AddButton'
],
function(
	Backbone
	, CompanyModel
	, CompaniesCollection
	, CompaniesView
	, SearchView
	, AddButtonView
) {

	var CompanyRouter = Backbone.Router.extend({

		ns: {
			collections: {}
			, views: {}
		}

		, initialize: function(options) {
			_.bindAll(this, '_renderCompanies');

			// DOM
			this.$header = options.$header;
			this.$el = options.$el;
			
			// Collections
			this.ns.collections.companies = new CompaniesCollection(null, {
				baseUrl: options.baseUrl || ''
			});
			
			// Views
			this.ns.views.companies = new CompaniesView({
				collection: this.ns.collections.companies
				, router: this
			});
			this.ns.views.search = new SearchView({
				collection: this.ns.collections.companies
			});
			this.ns.views.addBtn = new AddButtonView({
				router: this
			});

			this._renderSearchForm();
			this.$header.append( this.ns.views.addBtn.render().el );
		}

		, routes: {
			"":				"list"
			, "add":		"addCompany"
			, "edit/:id":	"editCompany"
		}

		, _renderSearchForm: function() {
			this.$header.append( this.ns.views.search.render().el );
		}

		, _renderCompanies: function() {
			this._render( this.ns.views.companies.render().el );
		}

		, _render: function(html) {
			this.$el.html( html );
		}

		, list: function() {
			if (!this.ns.collections.companies.size()) {
				this.ns.collections.companies.fetch({
						reset: true
						, success: this._renderCompanies
					});
			}
		}

		, addCompany: function() {
			var id;
			if (this.ns.collections.companies.size()) {
				id = this.ns.collections.companies.generateId();
				this.ns.collections.companies.add(
					new CompanyModel({
							id: id
							, editMode: true
						}
						, { at: id }
					)
				);
			} else {
				this.navigate('', { trigger: true });
			}
		}

		, editCompany: function(id) {
			var companyModel = this.ns.collections.companies.get(id);
			if (companyModel) {
				companyModel.set('editMode', true);
			} else {
				alert('Was not able to find specified company!');
				this.navigate('', { trigger: true });
			}
		}

	});

	return CompanyRouter;
});