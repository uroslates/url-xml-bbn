define([
	'backbone'
	, '../models/Company'
	, '../parsers/CompanyXMLParser'
],
function(
	Backbone
	, CompanyModel
	, CompanyXMLParser
) {

	var CompaniesCollection  = Backbone.Collection.extend({

		model: CompanyModel

		, url: [this.options && this.options.baseUrl ? this.options.baseUrl : "", "/js/data/companies.xml"].join('')

		, sync: function(method, model, options) {
			options = _.extend({
				dataType: "xml"
				, url: this.url
				, processData: false
			}, options);
			return Backbone.sync.call(this, method, model, options);
		}

		, parse: function(response) {
			return CompanyXMLParser.toJSON(response);
		}

		/**
		 * Comparator function.
		 * Inversing the order of companies (based on ID) to be able to 
		 * see the company that is to be added on top of the list.
		 */
		, comparator: function(company) {
			return -company.get('id');
		}

		/**
		 * Search collection based on provided filter
		 * @param  {Object} filter object used for collection searching
		 * @return {Array}        array of CompanyModel instances conforming to the search fitler
		 */
		, search: function(filter) {
			var results = [];

			if (filter) {
				results = this.searchByName(filter.name);
			}

			// Notify listeners
			this.trigger('search:done', results);
			return results;
		}

		/**
		 * Case insensitive search of a collection by name property.
		 * @param  {String} name name value to search by
		 * @return {Array}      array of CompanyModel instances conforming to the name
		 */
		, searchByName: function(name) {
			var results = [];
			
			if (name && $.trim(name).length) {
				results = this.filter(function(itemModel) {
					return itemModel.get('name').toLowerCase().indexOf(name.toLowerCase()) >= 0;
				});
			} else {
				results = this.map(function(item) { return item; });
			}

			return results;
		}

		/**
		 * ID generator for the collection.
		 * ID generted will be incremented value of highest ID.
		 * @return {[type]} [description]
		 */
		, generateId: function() {
			return parseInt(this.at(0).get('id'), 10) + 1;
		}

	});

	return CompaniesCollection;
});