define([
	'jquery'
	, 'backbone'
	, 'underscore'
	, 'text!../templates/search-form.tpl'
],
function(
	$
	, Backbone
	, _
	, SearchViewTpl
) {

	/**
	 * Company View
	 */
	var SearchView = Backbone.View.extend({

		tagName: 'form'

		, className: 'search pull-right'

		, template: _.template(SearchViewTpl)

		, events: {
			'keyup input[name="q"]': 'onSearch'
		}

		, render: function() {
			this.$el.html( this.template() );
			return this;
		}

		/**
		 * Method performing the companies collection search based
		 * on the provided seach term (confomring to company's name) by default.
		 * @param  {Event} e event object
		 */
		, onSearch: function(e) {
			var filter = { name: $.trim( this.$('input[name="q"]').val() ) };
			this.collection.search(filter);
			return false;
		}

	});

	return SearchView;
});