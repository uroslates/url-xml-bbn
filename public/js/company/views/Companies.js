define([
	'backbone'
	, 'underscore'
	, './Company'
	, 'text!../templates/table.tpl'
],
function(
	Backbone
	, _
	, CompanyView
	, CompaniesTpl
) {

	/**
	 * Companies View
	 */
	var CompaniesView = Backbone.View.extend({

		views: {}

		, template: _.template(CompaniesTpl)

		, initialize: function() {
			this.listenTo(this.collection, 'reset', this.render);
			this.listenTo(this.collection, 'add', this.onAdd);
			this.listenTo(this.collection, 'search:done', this.showFoundItems);
		}

		, render: function() {
			var itemView
				, $body = $('<tbody/>');

			this.$el.html( this.template() );
			
			this.collection.each(function(itemModel) {
				itemView = new CompanyView({ model: itemModel, router: this.options.router });
				this.views[itemView.cid] = itemView;
				// $body.append( itemView.render().el );
				this.$el.find('tbody').append( itemView.render().el );
			}, this);

			return this;
		}

		// Show/Hide rows based on thier model's visible value
		, showFoundItems: function(foundItems) {
			this.$el.removeClass('search-results').addClass('search-results');
			this.$('.company').removeClass('visible');
			_.each(foundItems, function(foundItem) {
				this.$el.find('.company.model-cid-'+foundItem.cid).addClass('visible');
			}, this);
		}

		, onAdd: function(model, collection, options) {
			var itemView = new CompanyView({ model: model, router: this.options.router });
			this.views[itemView.cid] = itemView;
			this.$el.find('tbody').prepend( itemView.render().el );
		}

		, remove: function() {
			_.keys(this.views).each(function(viewCid) {
				this.views[viewCid].remove();
			}, this);
		}

	});

	return CompaniesView;
});