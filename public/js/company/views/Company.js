define([
	'backbone'
	, 'underscore'
	, 'text!../templates/column.tpl'
	, 'text!../templates/column_edit.tpl'
],
function(
	Backbone
	, _
	, ColumnViewTpl
	, ColumnViewEditTpl
) {

	/**
	 * Company View
	 */
	var CompanyView = Backbone.View.extend({

		className: 'company'

		, tagName: 'tr'

		, template: function() {
			return _.template( this.model.get('editMode') ? ColumnViewEditTpl : ColumnViewTpl );
		}

		, events: {
			'keyup input[name="company[name]"]':	'onCompanyNameKeyUp'
		}

		, initialize: function() {
			_.bindAll(this, 'onCompanyNameKeyUp');
			this.listenTo(this.model, 'change:name', this.render);
			this.listenTo(this.model, 'change:editMode', this.onEditModeChange);
		}

		, onEditModeChange: function() {
			this.render();
			if (this.model.get('editMode')) {
				this.$('input[name="company[name]"]').focus();
			}
		}

		, render: function() {
			this.$el.html( this.template()(this.model.toJSON()) );
			this.addModelClass();
			return this;
		}

		, addModelClass: function() {
			this.$el.addClass(['model-cid-', this.model.cid].join(''));
		}

		, onCompanyNameKeyUp: function(e) {
			if (e.keyCode === 13 || e.keyCode === 27) {
				this.updateCompany();
			}
		}

		, updateCompany: function() {
			var $companyName = this.$('input[name="company[name]"]')
				, companyName = $.trim($companyName.val());
			this.model.set({
				editMode: false
				, name: companyName || $companyName.attr('placeholder')
			});
			this.options.router.navigate('', { trigger: true });
		}

	});

	return CompanyView;
});