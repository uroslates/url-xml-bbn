define([
	'backbone'
],
function(
	Backbone
) {

	/**
	 * Add Button View
	 */
	var AddButtonView = Backbone.View.extend({

		tagName: 'button'

		, className: 'btn add-btn. pull-left'

		, events: {
			'click': 'onAdd'
		}

		, render: function() {
			this.$el.text('Add');
			return this;
		}

		, onAdd: function(e) {
			this.options.router.navigate('add', { trigger: true });
		}

	});

	return AddButtonView;
});