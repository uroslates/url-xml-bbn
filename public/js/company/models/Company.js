define([
	'backbone'
],
function(
	Backbone
) {

	var CompanyModel = Backbone.Model.extend({

		defaults: {
			// Determining if a model should be visible or not (UI related)
			visible: true
			// Is edit mode or not
			, editMode: false
			, name: ''
		}

	});

	return CompanyModel;
});