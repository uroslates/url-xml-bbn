define([
	'jquery'
],
function(
	$
) {

	/**
	 * Intermal Company XML document parser.
	 * @type {Object}
	 */
	var CompanyXMLParser = {

		/**
		 * Method used to parse provided XML itto the JSON object.
		 * @param  {XML} xml XML document to parse
		 * @return {Object}     gennerated JSON Object
		 */
		toJSON: function(xml) {
			var companies = [];

			$(xml).find('company').each(function(i, company) {
				companies.push({
					id: $(company).attr('id')
					, name: $(company).find('name').text()
				});
			});

			return companies;
		}

	};

	return CompanyXMLParser;
});