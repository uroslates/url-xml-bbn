require([
    'jquery'
    , 'company/main'
], function(
    $
    , CompanyRouter
){

    var URL = {
        routers: {}
    };

    $(document).ready(function() {
        
        URL.routers.company = new CompanyRouter({
            $header: $('header')
            , $el: $('section.canvas')
            , baseUrl: "http://localhost:3000/"
        });

    });

    Backbone.history.start();
    // Expose as global object
    window.URL = URL;

});