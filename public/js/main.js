require.config({
  
    baseUrl:'js/'
  
    , paths : {
        jquery: 'libs/jquery/jquery.min'
        , underscore: 'libs/underscore/underscore'
        , backbone: 'libs/backbone/backbone-min'
        , text: 'libs/requirejs-text/text'
        // custom
        , company: 'company'
    }

    , shim: {
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        }
        , underscore: {
            exports: '_'
        }
        , d3: {
            exports: 'd3'
        }
    }

    , deps: ['bootstrap']
  
});